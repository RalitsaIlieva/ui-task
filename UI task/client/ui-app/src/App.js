import "./App.css";
import ItemsList from "./containers/ItemsList";

function App() {
  return (
    <div className="App">
      <ItemsList />
    </div>
  );
}

export default App;
