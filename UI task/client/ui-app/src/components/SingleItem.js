import React from "react";
import "./SingleItem.css";

const SingleItem = ({image, title, description, category}) => {
  return (
    <div class="item-container">
      <img class="image" src={image} alt="item"/>
      <div class="title">{title}</div>
      <div class="description">{description}</div>
      <div class="category">{category}</div>
    </div>
  );
};

export default SingleItem;
