import React, { useState, useEffect } from "react";
import { BASE_URL } from "../constants";
import SingleItem from "../components/SingleItem";
import './ItemsList.css';

const ItemsList = () => {
  const [items, setItems] = useState([]);

  useEffect(() => {
    fetch(`${BASE_URL}items`)
      .then((result) => result.json())
      .then((data) => {
        if (!Array.isArray(data)) {
          throw new Error("Something went wrong");
        }
        setItems(data);
      });
  }, []);

  if (items.length) {
    const transformedItems = items.map((i) => {
      return (
        <SingleItem
          key={i.id}
          id={i.id}
          image={i.image}
          title={i.title}
          description={i.description}
          category={i.category}
        />
      );
    });

    return <div class="container">{transformedItems}</div>;
  }
  return <div>Loading...</div>;
};

export default ItemsList;
